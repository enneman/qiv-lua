#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

extern void error(lua_State *, const char *, ...); 
extern void load(lua_State *, const char *); 
extern void call_va(const char*,const char *, ...);
extern void qiv_init_lua();
extern lua_State *L;
extern int file_exists(char *);
extern void checkluaconfig(void);
extern char *TRASH_DIR; 
extern char *SELECT_DIR; 

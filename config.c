/* $Id: config.c,v 1.48 2011/11/27 20:23:23 enneman Exp $ */
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>

/* lua specific */
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "qiv.h"
#include "config.h" 
int uselua = 0;
/* #define SELECT_DIR ".qiv-select"; */
/* #define TRASH_DIR ".qiv-trash";  */
char *SELECT_DIR;
char *TRASH_DIR;
lua_State *L; 
char *pathname;
char *localconfiglua=(char *)"qiv.lua";
char *globalconfiglua=(char *)"/etc/qiv/qiv.lua";
char *configlua;

void checkluaconfig(void)
{
/* TODO this could be more compact */
	if (file_exists(globalconfiglua) != 0)
	{
		fprintf(stderr,"qiv: using /etc/qiv/qiv.lua\n");
		uselua=1;
		configlua=globalconfiglua;
	}
	if (file_exists(localconfiglua) != 0)
	{
		fprintf(stderr,"qiv: using qiv.lua\n");
		uselua=1;
		configlua=localconfiglua;
	}
	if (uselua==1)
	{
		qiv_init_lua();
		load(L,(char const *)configlua);
		lua_getglobal(L,"TRASH_DIR");
		if(!lua_isstring(L,-1))
			error(L,"'TRASH_DIR' is not a string\n");
		TRASH_DIR = (char *)lua_tolstring(L,-1,(size_t *)((void *)0));

		lua_getglobal(L,"SELECT_DIR");
		if(!lua_isstring(L,-1))
			error(L,"'SELECT_DIR' is not a string\n");
		SELECT_DIR = (char *)lua_tolstring(L,-1,(size_t *)((void *)0)); 
/*TODO extend the configuration checks */


	}
	return;
}
/* check if file exists
 *
 * Note stat is overkill?
 * maybe use access()?
 */
int file_exists (char * fileName)
{
   struct stat buf;
   int i;
   int tmp; 

   tmp  = stat ((char const * __restrict ) fileName, (struct stat * __restrict )(&buf));
   i = tmp;
   /* File found */
   if ( i == 0 )
        {
	       return 1;
         }
        return 0;
			       
}


/* TODO first error then qiv-exit or the other way around? */
void error(lua_State *L, const char *fmt, ...) 
{
	/* assert(valid( L)); */

	va_list argp;
	va_start(argp, fmt);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	lua_close(L);
	/* close the main program */
	qiv_exit(EXIT_FAILURE);
	return;
}

/* Load the qiv configuration file */
void load(lua_State *L, const char *fname) 
{
	/* assert(valid( L)); */
	if (luaL_loadfilex(L,fname,(char const *)((void *)0)) || lua_pcallk(L,0,0,0,0, (int (*)(lua_State *L))((void *)0)))
		error(L,"cannot run config file: %s\n", lua_tostring(L, -1));
	return;
}

void qiv_init_lua()
{
	L = luaL_newstate();
	luaL_openlibs(L);
	return;
}

/* generic call function from "programming in lua" */
void call_va(const char *func, const char *sig, ...) 
{
	if (uselua==0) {
		return;
	}	
	va_list v1;
	int narg,nres; /* number of arguments and results */
	va_start(v1,sig);
	lua_getglobal(L, func);
	for (narg = 0; *sig; narg++) 
	{
		/* check stack space */
		luaL_checkstack(L, 1, "too many arguments");
		switch(*sig++)
		{
			case 'd': /* double argument */
				lua_pushnumber(L, va_arg(v1, double));
				break;
			case 'i': /* int argument */
				lua_pushinteger(L, (lua_Integer)va_arg(v1, int));
				break;
			case 's': /* string argument */
				lua_pushstring(L, (char const *)(va_arg(v1, char *)));
				break;
			case '>': /* end of arguments */
				goto endargs;
			default:
				error(L,"invalid option (%c)", (int const )*(sig -1));
		}
	}
	endargs:



	nres = (int) strlen(sig); /* number of expected result */


	/* do the call */
	if (lua_pcallk(L, narg, nres, 0,0,(int (*)(lua_State *L))(void *)0) != 0) /* do the call */
		error(L, "error calling '%s': %s", func, lua_tolstring(L, -1,(size_t *)((void *)0)));
	
	nres = -nres; /* stack index of first result */
	while (*sig)  /* repeat for each result */
	{
		switch (*sig++)
		{
			case 'd': /* double result */	
				if (!lua_isnumber(L,nres))
					error(L, "wrong result type");
				*va_arg(v1, double *) = lua_tonumberx(L,nres,(int *)((void *)0));
				break;
			case 'i': /* int result */

				if (!lua_isnumber(L,nres))
					error(L, "wrong result type");
				*va_arg(v1, int *) = lua_tointegerx(L,nres,(int *)((void *)0));
				break;
			case 's': /* string result */

				if (!lua_isstring(L,nres))
					error(L, "wrong result type");
				*va_arg(v1, const char **) = lua_tolstring(L,nres, (size_t *)((void *)0));
				break;
			default:
			       error(L, "invalid option (%c)", (int const )*(sig -1));
		}
		nres++;

	}	
	va_end(v1);

}


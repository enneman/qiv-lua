Intro

I was looking for a simple scriptable imageviewer.

My main criteria were: 
small
low overhead
no eye candy (extra menus or widgets)
simple

The first candidate was "fim" (http://code.autistici.org)
This viewer is _extremely_ scriptable through an internal domain specific language.
Sadly though, the documentation for this language is "somewhat" lacking :-(


After browsing through the code of several other viewers "fbi, feh, qiv..." 
I decided it was simpler to embed a scripting language I already knew in one 
of these viewers.

Lua is a script language that was designed to be "embedable" from the beginning.
And I had done some lua scripting before.
Qiv was the simplest of the viewers.

So I decided to try and embed lua in qiv.

The result was quite pleasing. Through lua I could acces the OS functions, and
also make database calls (luasql). Through the luarocks package manager I could
install virtually all lua libraries, and use these from qiv. Bliss!

At the moment I added a pre-, post- and exit hook to qiv. 
Qiv checks if the current directory has a qiv.lua file. If so it loads and compiles
this script.



Goals
- Make pre- and post-processing hooks (done, added an exit hook too)
- Make the trash directory configurable. (And more options in the main.h file?)
- If qiv.lua is missing, then qiv must run as was originally intended. (done)
- Identify all possible places in the code in which a hook could be added
- Hook/replace qiv-command with lua?
- TEST AS STANDALONE!!! lua loadpath !! (pending)
- Add lua to the project (and Makefile)
- Integration with Autotools. add a "withlua" target?


